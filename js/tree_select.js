(function($) {
  Drupal.behaviors.initTreeSelect = {
    attach: function(context) {
      for (propertyName in Drupal.settings.tree_select_dropdown) {
        $('.' + propertyName + ' .tree-select-dropdown-value').once('tree-select-dropdown-init', function() {
          $(this).mcDropdown('.' + propertyName + ' .hierarchical-list', Drupal.settings.tree_select_dropdown[propertyName]);
        });
      }
    }
  }
  Drupal.behaviors.addClassToParent = {
    attach: function(context) {
      $(document).delegate('.tree-select-dropdown-value', 'focus', function() {
        $(this).parents('div.mcdropdown').addClass('active');
      });
      $(document).delegate('.tree-select-dropdown-value', 'blur', function() {
        $(this).parents('div.mcdropdown').removeClass('active');
      });
      $(document).delegate('a', 'focus', function() {
        $(this).parents('div.mcdropdown').addClass('active');
      });
      $(document).delegate('a', 'blur', function() {
        $(this).parents('div.mcdropdown').removeClass('active');
      });
    }
  }
})(jQuery);
