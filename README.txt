Readme file for the Tree Select module for Drupal
---------------------------------------------

This module introduces a form element and a field widget which provide a
flexible way for choosing an item from an hierarchy. It is based on a jQuery
component http://www.givainc.com/labs/mcdropdown_jquery_plugin.htm

Installation
------------

1. Extract mcDropdown jQuery Plug-in archive in the following (recommended)
   location: sites/all/libraries

   So the actual plugin can be found at:
   sites/all/libraries/jquery.mcdropdown/lib/jquery.mcdropdown.js

2. Go to "Modules" and enable the module.
-----------

Form element is created just like any other element but 'tree_select_dropdown'
type is used:

  $form['test-element'] = array(
    '#type' => 'tree_select_dropdown',
    '#mcdropdown_options' => array(), // An hierarchical nested array with all
                                      //options, see format below.
    '#mcdropdown' => array(...), // An array with all mcdropdown settings.
                                 // See description below.
  );
A field widget is called 'Tree select' and could be chosen for Taxonomy Term
Reference field. Other fields are not supported now.

ATTENTION: Field widget doesn't work in a multiple mode now.

Example :

$form['test_element'] = array(
  '#type'    => 'tree_select_dropdown',
  '#mcdropdown_options' => array(
    "label" => "",
    "children" => array(
      1 => array(
        "label" => "Box 1",
        "children" => array(
          2 => array(
            "label" => "Box 1.1",
            "children" => array(
              3 => array(
                "label" => "Box 1.1.1",
                "children" => array(),
              ),
              4 => array(
                "label" => "Box 1.1.2",
                "children" => array(),
              ),
            ),
          ),
          5 => array(
            "label" => "Box 1.2",
            "children" => array(),
          ),
          6 => array(
            "label" => "Box 1.2",
            "children" => array(),
          ),
        ),
      ),
      7 => array(
        "label" => "Box 2",
        "children" => array(),
      ),
    ),
  ),
  '#mcdropdown'    => array(
    'minRows' => 2,
    'maxRows' => 2,
    'targetColumnSize' => 1,
    'targetColumnSize' => 2,
    'openFx' => 'fadeIn',
    'openSpeed' => 600,
    'closeFx' => 'fadeOut',
    'closeSpeed' => 600,
    'hoverOverDelay' => 20,
    'hoverOutDelay' => 200,
    'showFx' => 'show',
    'showSpeed' => 200,
    'hideFx' => 'hide',
    'hideSpeed' => 200,
    'dropShadow' => true,
    'autoHeight' => true,
    'dropShadow' => false,
    'autoHeight' => false,
    'lineHeight' => 19,
    'screenPadding' => 10,
    'allowParentSelect' => true,
    'delim' => '  =>  ',
    'showACOnEmptyFocus' => false,
    'valueAttr' => "rel",
    'click' => null,
    'select' => null,
    'init' => null,
  ),
);
